import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule }    from '@angular/common/http';
import { CommonUtilService } from 'src/app/services/CommonUtilService';
import { AppRoutingModule } from './app.routing';
import { DataService } from './services/data.service';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule  

  ],
  providers: [CommonUtilService,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
