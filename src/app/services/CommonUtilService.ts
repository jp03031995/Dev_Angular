import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ng2DeviceService } from 'ng2-device-detector';
@Injectable()
export class CommonUtilService {
  constructor(private deviceService: Ng2DeviceService, private https:HttpClient) { }

  private GetIpAddress(DeviceInfo: any) {
    this.https.get<{ ip: string; }>('https://jsonip.com')
      .subscribe(data => {
        DeviceInfo.createdip = data.ip;
      });
  }
  getDeviceInformation() {
    let DeviceInformation;
    let DeviceInfo: any = {
      "createdip": "",
      "browser": "",
      "os": "",
      "macaddress": "",
      "latlong": "",
      "source": ""
    };
    DeviceInformation = this.deviceService.getDeviceInfo();
    this.GetIpAddress(DeviceInfo);
    this.getLocation(DeviceInfo);
    DeviceInfo.browser = DeviceInformation.browser;
    DeviceInfo.os = DeviceInformation.os;
    DeviceInfo.source = DeviceInformation.os_version;
    return DeviceInfo;
  }
  private getLocation(deviceinfo) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          deviceinfo.latlong = position.coords.latitude + ',' + position.coords.longitude;
        }
      },
        (error: PositionError) => console.log(error, 'im error'));
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }



}