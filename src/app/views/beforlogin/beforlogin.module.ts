import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { BeforRoutingModule } from './beforlogin-routing.module';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { VerifyUserComponent } from './verify-users/verify-users.component';

  
@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    VerifyUserComponent


  ],
  imports: [
    BeforRoutingModule,
    FormsModule,
    HttpClientModule ,
    ReactiveFormsModule 

  ]
})
export class BeforLoginModule { }
